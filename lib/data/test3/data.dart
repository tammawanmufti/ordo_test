import 'package:flutter/material.dart';

abstract class TestThreeData {
  static const Map<String, IconData> navbar = {
    'home': Icons.home,
    'list': Icons.list,
    'restore': Icons.restore,
  };

  static const String profilePicture = 'asset/images/team-2.png';

  static const Map<String, String> profileData = {
    "Rating": "5.0",
    "Review": "100",
    "Pesanan": "165",
  };

  static const String profileName = 'Dina Florist';

  static const String profileDescription =
      '''Toko Bunga terbaiks se Indonesia Raya 
Harga Murah Produk Berkualitas''';

  static const List<String> images = [
    'asset/images/test2/flower1.png',
    'asset/images/test2/flower2.png',
    'asset/images/test2/flower3.png',
  ];
}
