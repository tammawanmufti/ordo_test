import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';

abstract class TestTwoData {
  static const Map<String, IconData> navbar = {
    'chrome': Icons.chrome_reader_mode,
    'swap': Icons.swap_horiz,
    'move': Icons.move_to_inbox,
    'assesment': Icons.assessment,
    'racing': CommunityMaterialIcons.racing_helmet
  };

  static const String profilePicture = 'asset/images/team-2.png';

  static const Map<String, String> profileData = {
    "Rating": "5.0",
    "Review": "100",
    "Pesanan": "165",
  };

  static const String profileName = 'Dina Florist';

  static const String profileDescription =
      '''Toko Bunga terbaiks se Indonesia Raya 
Harga Murah Produk Berkualitas''';

  static const List<String> images = [
    'asset/images/test2/flower1.png',
    'asset/images/test2/flower2.png',
    'asset/images/test2/flower3.png',
  ];
}
