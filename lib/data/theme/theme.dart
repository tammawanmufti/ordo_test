import 'package:flutter/material.dart';

abstract class AppTheme {
  static ThemeData test_1 = initial.copyWith(primaryColor: Color(0xFF47623F));
  static ThemeData test_2 = initial.copyWith(primaryColor: Colors.black);
  static ThemeData test_3 = initial.copyWith(
      primaryColor: Color(0xFFFF9A00), accentColor: Color(0xFF53B2FC));
  static ThemeData initial =
      ThemeData(fontFamily: 'Poppins', backgroundColor: Color(0xFFFCF8F8));
}

abstract class AppGradient {
  static Gradient test_1 = LinearGradient(
    end: Alignment.bottomLeft,
    begin: Alignment.topRight,
    colors: [
      Color(0xFF3AB648),
      Color(0xFF526430),
    ],
  );

  static Gradient test_2 = LinearGradient(
    colors: [
      Color(0xFF400000),
      Color(0xFF202237),
      Color(0xFF595FA0),
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );

  static Gradient test_3 = LinearGradient(
    colors: [
      Color(0xFF53B2FC),
      Color(0xFF127CCE),
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );
}

abstract class AppShadow {
  static List<BoxShadow> elevate = [
    BoxShadow(
        offset: Offset(0, 10),
        color: Colors.black.withOpacity(0.3),
        blurRadius: 40)
  ];
}
