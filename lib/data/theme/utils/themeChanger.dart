import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class ThemeUtil extends GetxController {
  var theme = ThemeData().obs;
  void changeTheme(ThemeData themeData) {
    Get.changeTheme(themeData);
  }
}
