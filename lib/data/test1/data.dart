import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';

abstract class TestOneData {
  static const Map<String, IconData> navbar = {
    'assignment': Icons.assignment,
    'swap': Icons.swap_horiz,
    'home': Icons.home,
    'bell': CommunityMaterialIcons.bell_outline,
    'person': Icons.person_outline
  };
}
