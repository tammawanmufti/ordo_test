import 'package:flutter/material.dart';

class ScreenProp {
  static late double height;
  static late double width;

  static void init(MediaQueryData mediaQueryData) {
    height = mediaQueryData.size.height;
    width = mediaQueryData.size.width;
  }
}
