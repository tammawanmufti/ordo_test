import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:ordo_test/app/portal/portal.dart';
import 'package:ordo_test/data/theme/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Ordo Assesment',
      debugShowCheckedModeBanner: false,
      theme: AppTheme.initial,
      home: PortalPage(),
    );
  }
}
