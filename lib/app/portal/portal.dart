import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ordo_test/app/test_1/testOne.dart';
import 'package:ordo_test/app/test_2/testTwo.dart';
import 'package:ordo_test/app/test_3/testThree.dart';
import 'package:ordo_test/app/widget/button/roundedTextButton.dart';
import 'package:ordo_test/data/const/basic.dart';
import 'package:ordo_test/data/theme/theme.dart';
import 'package:ordo_test/device/utils/sceenProp.dart';

class PortalPage extends StatelessWidget {
  const PortalPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenProp.init(MediaQuery.of(context));
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Made By',
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                BasicData.name,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
              SizedBox(
                height: 24,
              ),
              RoundedTextButton(
                color: Colors.black,
                onPressed: () {
                  Get.changeTheme(AppTheme.test_1);
                  Get.to(TestOneApp());
                },
                label: 'Test 1',
              ),
              SizedBox(
                height: 16,
              ),
              RoundedTextButton(
                color: Colors.black,
                onPressed: () {
                  Get.changeTheme(AppTheme.test_2);
                  Get.to(TestTwoApp());
                },
                label: 'Test 2',
              ),
              SizedBox(
                height: 16,
              ),
              RoundedTextButton(
                color: Colors.black,
                onPressed: () {
                  Get.changeTheme(AppTheme.test_3);
                  Get.to(TestThreeApp());
                },
                label: 'Test 3',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
