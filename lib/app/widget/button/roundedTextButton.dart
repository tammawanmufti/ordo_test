import 'package:flutter/material.dart';

class RoundedTextButton extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;
  final double labelSize;
  final Color color;
  final Size minSize;
  const RoundedTextButton(
      {Key? key,
      this.label = '',
      required this.onPressed,
      this.labelSize = 16,
      this.color = Colors.blue,
      this.minSize = const Size(160, 40)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      key: key,
      style: ButtonStyle(
          minimumSize: MaterialStateProperty.all(this.minSize),
          padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(horizontal: 24)),
          backgroundColor: MaterialStateProperty.all(this.color),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50.0),
          ))),
      onPressed: onPressed,
      child: Text(
        label,
        style: TextStyle(fontSize: this.labelSize),
      ),
    );
  }
}
