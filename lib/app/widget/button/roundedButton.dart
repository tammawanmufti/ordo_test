import 'package:flutter/material.dart';
import 'package:ordo_test/device/utils/sceenProp.dart';

class RoundedButton extends StatelessWidget {
  final Widget? child;
  final VoidCallback onPressed;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final double labelSize;
  final double borderRadius;
  final Gradient? gradient;
  final Color color;
  final double? width;
  const RoundedButton({
    Key? key,
    this.child,
    required this.onPressed,
    this.labelSize = 16,
    this.color = Colors.blue,
    this.width,
    this.borderRadius = 25,
    this.padding,
    this.margin,
    this.gradient,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin ?? EdgeInsets.zero,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.zero,
            visualDensity: VisualDensity(horizontal: 0, vertical: -3),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20))),
        child: Ink(
          decoration: BoxDecoration(
              color: color,
              gradient: gradient,
              borderRadius: BorderRadius.circular(20)),
          child: Container(
            padding: padding,
            width: width ??
                ScreenProp.width - (margin?.left ?? 0) - (margin?.right ?? 0),
            height: 55,
            alignment: Alignment.center,
            child: child,
          ),
        ),
      ),
    ); //     )));
  }
}
