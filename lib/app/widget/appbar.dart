import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class CustomWidget {
  static AppBar appbar(BuildContext context,
          {String title = '', List<Widget>? actions}) =>
      AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: actions,
        actionsIconTheme:
            IconThemeData(color: Theme.of(context).primaryColor, size: 20),
        title: Text(
          title,
          style: TextStyle(
              color: Colors.black, fontSize: 13, fontWeight: FontWeight.bold),
        ),
        leading: IconButton(
          onPressed: Get.back,
          icon: Container(
            padding: EdgeInsets.all(6),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              shape: BoxShape.circle,
            ),
            child: Icon(
              FontAwesomeIcons.arrowLeft,
              color: Colors.white,
              size: 10,
            ),
          ),
        ),
      );
}
