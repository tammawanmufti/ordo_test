import 'package:flutter/material.dart';

class AppBottomNavbar extends StatelessWidget {
  final List<BottomNavigationBarItem> items;
  final ValueChanged<int>? onTap;
  final int currentIndex;
  const AppBottomNavbar({
    Key? key,
    required this.items,
    this.onTap,
    this.currentIndex = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 85,
      child: BottomNavigationBar(
          selectedItemColor: Theme.of(context).primaryColor,
          backgroundColor: Colors.white,
          elevation: 0,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          onTap: onTap,
          currentIndex: currentIndex,
          unselectedItemColor: Color(0xFFC6C4C4),
          items: items),
    );
  }
}
