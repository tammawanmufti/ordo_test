import 'package:flutter/material.dart';

abstract class AppColor {
  static Map<int, Color> _purple = {
    50: Color.fromRGBO(49, 45, 54, 1),
    100: Color.fromRGBO(64, 55, 73, 1),
    200: Color.fromRGBO(80, 67, 93, 1),
    300: Color.fromRGBO(95, 78, 113, 1),
    400: Color.fromRGBO(110, 89, 133, 1),
    500: Color.fromRGBO(126, 101, 153, 1),
    600: Color.fromRGBO(142, 122, 174, 1),
    700: Color.fromRGBO(157, 124, 193, 1),
    800: Color.fromRGBO(175, 135, 213, 1),
    900: Color.fromRGBO(188, 146, 233, 1),
  };

  static Map<int, Color> _darkPurple = {
    50: Color(0xFF7952B3),
    100: Color(0xFF7952B3),
    200: Color(0xFF7952B3),
    300: Color(0xFF7952B3),
    400: Color(0xFF7952B3),
    500: Color(0xFF7952B3),
    600: Color(0xFF7952B3),
    700: Color(0xFF7952B3),
    800: Color(0xFF7952B3),
    900: Color(0xFF7952B3),
  };

  static MaterialColor purple = MaterialColor(0xFFC490E4, _purple);
  static MaterialColor darkPurple = MaterialColor(0xFF7952B3, _darkPurple);
}
