import 'package:flutter/material.dart';

class SmallButton extends StatelessWidget {
  final Color? color;
  final String? label;
  final Widget? child;
  final VoidCallback onPressed;

  const SmallButton({
    Key? key,
    this.color,
    this.label,
    this.child,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            visualDensity: VisualDensity.compact,
            fixedSize: Size(77, 26),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0)),
            primary: color ?? Theme.of(context).primaryColor),
        onPressed: () {},
        child: child ??
            Text(
              label!,
              style: TextStyle(fontSize: 9, fontWeight: FontWeight.w500),
            ));
  }
}
