import 'package:flutter/material.dart';
import 'package:ordo_test/app/test_3/widgets/smallButton.dart';

class RebateHeader extends StatelessWidget {
  const RebateHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 17.0, right: 17.0, top: 15.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Daftar Rebate',
            style: TextStyle(
              color: Color(0xFF94AFB6),
              fontSize: 11.0,
              fontWeight: FontWeight.w400,
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SmallButton(
                label: 'Semua',
                onPressed: () {},
              ),
              SizedBox(width: 10),
              SmallButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      'Filter',
                      style:
                          TextStyle(fontSize: 9, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 16,
                      width: 16,
                      child: Image.asset(
                        'asset/images/options.png',
                        fit: BoxFit.cover,
                      ),
                    )
                  ],
                ),
                color: Theme.of(context).accentColor,
                onPressed: () {},
              ),
            ],
          )
        ],
      ),
    );
  }
}
