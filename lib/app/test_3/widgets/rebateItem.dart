import 'package:flutter/material.dart';

class RebateItem extends StatelessWidget {
  const RebateItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RebateCell(
            header: 'ID Transaksi',
            item: Text(
              '#EC1201211',
              style: TextStyle(
                fontSize: 10,
                color: Color(0xFF3D6670),
              ),
            ),
          ),
          RebateCell(
            header: 'Rebate',
            item: Text(
              '#EC1201211',
              style: TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.w600,
                color: Color(0xFF41BE06),
              ),
            ),
          ),
          RebateCell(
            header: 'Tanggal',
            item: Text(
              '14 Juli 2017',
              style: TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.w600,
                color: Color(0xFF41BE06),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class RebateCell extends StatelessWidget {
  final String header;
  final Widget item;

  const RebateCell({
    Key? key,
    required this.header,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          header,
          style: TextStyle(
            fontSize: 10,
            color: Color(0xFF94AFB6),
          ),
        ),
        item
      ],
    );
  }
}
