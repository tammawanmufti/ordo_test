import 'package:flutter/material.dart';
import 'package:ordo_test/data/theme/theme.dart';

class RiwayatRebate extends StatelessWidget {
  const RiwayatRebate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                shape: BoxShape.circle, gradient: AppGradient.test_3),
            child: Icon(
              Icons.receipt,
              color: Colors.white,
              size: 20,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '#REBATEC120211',
                    style: TextStyle(
                      fontSize: 10,
                      color: Color(0xFF212529),
                    ),
                  ),
                  Text(
                    '20 Juli 2021',
                    style: TextStyle(
                      fontSize: 10,
                      color: Color(0xFF6C757D),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.orange,
              borderRadius: BorderRadius.circular(25),
            ),
            child: Text(
              'Rp 150.000',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.white,
                fontSize: 11,
              ),
            ),
          )
        ],
      ),
    );
  }
}
