import 'package:flutter/material.dart';
import 'package:ordo_test/data/theme/theme.dart';

class BonusCard extends StatelessWidget {
  final String title;
  final String subitle;
  const BonusCard({
    this.title = '',
    this.subitle = '',
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 24, vertical: 12),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 18),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: AppShadow.elevate,
            borderRadius: BorderRadius.circular(20)),
        child: ListTile(
          title: Text(
            this.title,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w700,
                color: Theme.of(context).primaryColor),
          ),
          subtitle: Text(this.subitle,
              style: TextStyle(
                fontSize: 21,
                fontWeight: FontWeight.w700,
              )),
        ));
  }
}
