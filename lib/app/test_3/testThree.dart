import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ordo_test/app/test_3/widgets/bonusCard.dart';
import 'package:ordo_test/app/test_3/widgets/rebateHeader.dart';
import 'package:ordo_test/app/test_3/widgets/rebateItem.dart';
import 'package:ordo_test/app/test_3/widgets/riwayatRebate.dart';
import 'package:ordo_test/app/widget/appbar.dart';
import 'package:ordo_test/app/widget/button/roundedButton.dart';
import 'package:ordo_test/app/widget/icon/gradientIcon.dart';
import 'package:ordo_test/app/widget/navbar/appBottomNavbar.dart';
import 'package:ordo_test/data/test3/data.dart';
import 'package:ordo_test/data/theme/theme.dart';

class TestThreeApp extends StatelessWidget {
  const TestThreeApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidget.appbar(
        context,
        title: 'Finansial',
        actions: [
          IconButton(
            onPressed: () {},
            icon: GradientIcon(
              Icons.assessment,
              24,
              AppGradient.test_3,
            ),
          ),
          IconButton(
            onPressed: () {
              print(pi / 1);
            },
            icon: Stack(
              children: [
                Transform.rotate(
                  angle: -pi / 15,
                  child: GradientIcon(
                    FontAwesomeIcons.solidBell,
                    24,
                    AppGradient.test_3,
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: 16,
                    width: 16,
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      border: Border.all(color: Colors.white, width: 2),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: BonusCard(
                  title: 'Total Bonus',
                  subitle: 'Rp 5.000.000,00',
                ),
              ),
              SliverToBoxAdapter(
                child: BonusCard(
                  title: 'Pending Bonus',
                  subitle: 'Rp 50.000,00',
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                  // padding: EdgeInsets.symmetric(horizontal: 20, vertical: 18),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  child: Column(children: [
                    RebateHeader(),
                    Divider(
                      color: Color(0xFFBBBBBB),
                    ),
                    ...List.generate(6, (index) => RebateItem())
                  ]),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                    margin: const EdgeInsets.symmetric(
                        horizontal: 24, vertical: 12),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20)),
                    child: Container(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Text(
                            'Riwayat Rebate',
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 13,
                                color: Theme.of(context).primaryColor),
                          ),
                        ),
                        ...List.generate(6, (index) => RiwayatRebate())
                      ],
                    ))),
              )
            ],
          ),
          Positioned(
            bottom: 25,
            child: RoundedButton(
              onPressed: () {},
              gradient: AppGradient.test_3,
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              margin: EdgeInsets.symmetric(horizontal: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 30,
                  ),
                  Text(
                    'Complain',
                    style: TextStyle(fontSize: 13, fontWeight: FontWeight.w700),
                  ),
                  Icon(Icons.arrow_right_alt_outlined)
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: AppBottomNavbar(
          currentIndex: 0,
          items: TestThreeData.navbar.entries
              .map(
                (e) =>
                    BottomNavigationBarItem(icon: Icon(e.value), label: e.key),
              )
              .toList()),
    );
  }
}
