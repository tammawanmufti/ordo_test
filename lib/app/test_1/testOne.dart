import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ordo_test/app/test_1/widgets/map.dart';
import 'package:ordo_test/app/widget/appbar.dart';
import 'package:ordo_test/app/widget/icon/gradientIcon.dart';
import 'package:ordo_test/data/theme/theme.dart';

class TestOneApp extends StatelessWidget {
  const TestOneApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidget.appbar(context, title: 'LACAK PESANAN', actions: [
        IconButton(
          onPressed: () {},
          icon: GradientIcon(
            FontAwesomeIcons.shoppingBag,
            24,
            AppGradient.test_1,
          ),
        )
      ]),
      body: Stack(
        fit: StackFit.expand,
        children: [
          AppMap(),
          DraggableScrollableSheet(
            initialChildSize: 0.3,
            minChildSize: 0.3,
            maxChildSize: 1,
            builder: (BuildContext context, ScrollController scrollController) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
                  color: Colors.white,
                ),
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8, vertical: 0),
                      child: ListView(controller: scrollController, children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 40),
                          child: Row(
                            children: [
                              Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                          AssetImage('asset/images/photo.jpeg'),
                                      fit: BoxFit.contain),
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      style: BorderStyle.solid,
                                      color: Colors.green,
                                      width: 4),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 16.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'David Morel',
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.grey[900],
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        'B 1201 FA',
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Color(0xFF92D274),
                                            fontWeight: FontWeight.w700),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              IconButton(
                                iconSize: 60,
                                onPressed: () {},
                                icon: Container(
                                    padding: EdgeInsets.all(12),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        gradient: AppGradient.test_1),
                                    child: Icon(
                                      Icons.message,
                                      size: 30,
                                      color: Colors.white,
                                    )),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 24, vertical: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TimelineItem(
                                icon: FontAwesomeIcons.clock,
                                title: 'Status',
                                description: 'Sedang mengambil barang',
                              ),
                              DotDivider(),
                              TimelineItem(
                                icon: Icons.location_on,
                                title: "Alamat Anda",
                                description: "Taman Indah Dago No 2",
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Title(
                              color: Colors.black,
                              child: Text('Pesanan',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14,
                                  )),
                            )),
                        Column(children: [
                          PesananItem(),
                          PesananItem(),
                        ]),
                        CatatanPesanan(),
                      ]),
                    ),
                    Positioned(
                      top: 8,
                      left: 0,
                      right: 0,
                      child: Center(
                        child: Container(
                          width: 100,
                          height: 4,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(30)),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          )
        ],
      ),
    );
  }
}

class CatatanPesanan extends StatelessWidget {
  const CatatanPesanan({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Catatan Pesanan',
            style: TextStyle(
                fontSize: 14, color: Colors.black, fontWeight: FontWeight.w600),
          ),
          Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ullamco laboris nisi ut aliquip ex ea commodo .',
            style: TextStyle(
                fontSize: 11,
                color: Color(0xFF8A7F7F),
                fontWeight: FontWeight.w400),
          ),
        ],
      ),
    );
  }
}

class PesananItem extends StatelessWidget {
  const PesananItem({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Row(
        children: [
          Container(
            height: 78,
            width: 78,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white.withOpacity(0.9),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12, blurRadius: 30, spreadRadius: 1)
              ],
              image: DecorationImage(
                fit: BoxFit.contain,
                image: AssetImage('asset/images/strwberry.png'),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Strawberry',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                      )),
                  Text(
                    '100 Gram',
                    style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF979696)),
                  ),
                  Text(
                    'Rp 75.000',
                    style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFF47623F)),
                  ),
                ],
              ),
            ),
          ),
          Text('2 Item',
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w700))
        ],
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Catatan Item',
            style: TextStyle(
                fontSize: 11, color: Colors.black, fontWeight: FontWeight.w600),
          ),
          Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ullamco laboris nisi ut aliquip ex ea commodo .',
            style: TextStyle(
                fontSize: 11,
                color: Color(0xFF8A7F7F),
                fontWeight: FontWeight.w400),
          ),
          Divider()
        ],
      ),
    );
  }
}

class TimelineItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final String description;
  const TimelineItem({
    Key? key,
    required this.icon,
    required this.title,
    required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          icon,
          color: Color(0xFF92D274),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 14),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontSize: 14,
                  color: Color(0xFF47623F),
                ),
              ),
              Text(
                description,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  color: Color(0xFF061737),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class DotDivider extends StatelessWidget {
  const DotDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          height: 4,
          width: 4,
          decoration: BoxDecoration(
              color: Colors.blueGrey[300], shape: BoxShape.circle),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          height: 5,
          width: 5,
          decoration: BoxDecoration(
              color: Colors.blueGrey[300], shape: BoxShape.circle),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          height: 6,
          width: 6,
          decoration: BoxDecoration(
              color: Colors.blueGrey[300], shape: BoxShape.circle),
        ),
      ],
    );
  }
}
