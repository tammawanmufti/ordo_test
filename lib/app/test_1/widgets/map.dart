import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong2/latlong.dart';
import 'package:ordo_test/device/utils/sceenProp.dart';

class AppMap extends StatefulWidget {
  const AppMap({Key? key}) : super(key: key);

  @override
  _AppMapState createState() => _AppMapState();
}

class _AppMapState extends State<AppMap> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenProp.height,
      width: ScreenProp.width,
      color: Colors.indigo.withOpacity(.3),
      child: FlutterMap(
        options: MapOptions(
          center: LatLng(-6.229728, 106.6894298),
          zoom: 16.0,
        ),
        layers: [
          TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c']),
          MarkerLayerOptions(
            markers: [
              Marker(
                height: 20,
                width: 20,
                point: LatLng(-6.229728, 106.6894298),
                builder: (ctx) => Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Theme.of(context).primaryColor,
                            blurRadius: 14,
                            spreadRadius: 1)
                      ],
                      border: Border.all(color: Colors.white, width: 2),
                      shape: BoxShape.circle,
                      color: Theme.of(context).primaryColor),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
