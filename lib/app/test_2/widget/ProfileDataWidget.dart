import 'package:flutter/material.dart';
import 'package:ordo_test/app/test_2/widget/ProfileDataItem.dart';
import 'package:ordo_test/data/test2/data.dart';

class ProfileDataWidget extends StatelessWidget {
  const ProfileDataWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          height: 87,
          width: 87,
          child: CircleAvatar(
            child: Image.asset(TestTwoData.profilePicture),
            backgroundColor: Colors.white,
          ),
        ),
        ...TestTwoData.profileData.entries
            .map((e) => ProfileDataItem(data: e))
            .toList()
      ],
    );
  }
}
