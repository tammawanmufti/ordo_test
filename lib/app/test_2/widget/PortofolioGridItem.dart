import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:ordo_test/data/theme/theme.dart';

class PortofolioGridItem extends StatelessWidget {
  final String name;
  final ImageProvider image;
  final double rate;
  const PortofolioGridItem({
    Key? key,
    required this.name,
    required this.image,
    required this.rate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(1),
      height: 150,
      decoration: BoxDecoration(
          color: Colors.grey,
          image: DecorationImage(image: image, fit: BoxFit.cover)),
      child: Stack(
        children: [
          Positioned(
            right: 8,
            top: 7,
            child: Container(
              width: 38,
              decoration: BoxDecoration(
                  gradient: AppGradient.test_2,
                  borderRadius: BorderRadius.circular(50)),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(
                    CommunityMaterialIcons.star,
                    size: 12,
                    color: Color(0xFFDFB300),
                  ),
                  Text(
                    rate.toStringAsFixed(1),
                    style: TextStyle(fontSize: 10, color: Colors.white),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              padding: EdgeInsets.only(left: 3.75),
              height: 20,
              width: 300,
              color: Colors.black26,
              alignment: Alignment.centerLeft,
              child: Text(
                name,
                style: TextStyle(
                    fontSize: 10,
                    color: Colors.white,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
