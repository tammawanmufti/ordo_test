import 'package:flutter/material.dart';

class ProfileDataItem extends StatelessWidget {
  final MapEntry<String, String> data;
  const ProfileDataItem({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(data.value,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
            )),
        Text(data.key,
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400)),
      ],
    );
  }
}
