import 'dart:math';

import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ordo_test/app/test_2/widget/PortofolioGridItem.dart';
import 'package:ordo_test/app/test_2/widget/ProfileDataWidget.dart';
import 'package:ordo_test/app/widget/appbar.dart';
import 'package:ordo_test/app/widget/icon/gradientIcon.dart';
import 'package:ordo_test/app/widget/navbar/appBottomNavbar.dart';
import 'package:ordo_test/data/test2/data.dart';
import 'package:ordo_test/data/theme/theme.dart';

class TestTwoApp extends StatelessWidget {
  const TestTwoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Random random = Random();

    return Scaffold(
        appBar:
            CustomWidget.appbar(context, title: 'PORTOFOLIO VENDOR', actions: [
          IconButton(
              onPressed: () {},
              icon: GradientIcon(
                  FontAwesomeIcons.solidBell, 20, AppGradient.test_2))
        ]),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18),
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                  child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 22.5),
                    child: ProfileDataWidget(),
                  ),
                  ListTile(
                    contentPadding: const EdgeInsets.only(top: 9),
                    title: Text(
                      TestTwoData.profileName,
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
                    ),
                    subtitle: Text(
                      TestTwoData.profileDescription,
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    ),
                  ),
                  OutlinedButton(
                    style: ButtonStyle(
                        elevation: MaterialStateProperty.all(0),
                        backgroundColor:
                            MaterialStateProperty.all(Colors.white),
                        minimumSize: MaterialStateProperty.all(
                            Size(double.infinity, 29))),
                    onPressed: () {},
                    child: Text(
                      'PORTOFOLIO',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                  )
                ],
              )),
              SliverGrid.count(
                crossAxisCount: 3,
                children: List.generate(
                  40,
                  (index) => PortofolioGridItem(
                    rate: random.nextInt(5).toDouble() + random.nextDouble(),
                    name: faker.person.name(),
                    image: AssetImage(TestTwoData.images[random.nextInt(3)]),
                  ),
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: AppBottomNavbar(
          currentIndex: 0,
          items: TestTwoData.navbar.entries
              .map((e) => BottomNavigationBarItem(
                  icon: Icon(
                    e.value,
                    size: 24,
                  ),
                  label: e.key))
              .toList(),
        ));
  }
}
